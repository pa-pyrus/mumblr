# Mumblr IRC <-> Mumble Relay #
Mumblr is a *simple* relay for text messages between an **IRC** channel and a **Mumble** server.

## Setup ##
1. Install **Node.js** and **[npm](https://www.npmjs.org/)**.
2. Run `npm install [-g] git+https://bitbucket.org/pa-pyrus/mumblr.git` to install Mumblr.
3. Adapt configuration in `~/.config/Mumblr/config.json`.
   An example is provided in `config.json`.
4. Run `npm start mumblr`.

## Dependencies ##
* [Node.js](http://www.nodejs.org/)
    * All other dependencies will be installed using **[npm](https://www.npmjs.org/)**.
* [CoffeeScript](http://coffeescript.org/)
* [node-mumble](https://github.com/Rantanen/node-mumble)
    * node-mumble (or rather its dependency *protobuf*) depends on a **python2** interpreter.
      When in doubt, set the `PYTHON` environment variable accordingly.
* [node-irc](https://github.com/martynsmith/node-irc)
