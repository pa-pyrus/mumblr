fs = require "fs"
path = require "path"

nconf = require "nconf"
irc = require "irc"
mumble = require "mumble"

# read configuration
nconf.file path.join process.env.HOME, ".config/Mumblr/config.json"
NICKNAME = nconf.get "nickname"
IRC_OPTS = nconf.get "irc"
MUMBLE_OPTS = nconf.get "mumble"


ircClient = new irc.Client IRC_OPTS.server, NICKNAME,
  channels: [ IRC_OPTS.channel ]
  userName: "mumblr"
  realName: "Mumblr IRC-Mumble Bridge"

ircClient.addListener "message#{IRC_OPTS.channel}", (from, message) ->
  console.log "[I] Message from #{from}: #{message}"
  if mumbleConnection?
    console.log "[F] Forwarding to Mumble..."
    session = mumbleConnection.sessionId
    user = mumbleConnection.users[session]
    channel = user.channelId
    mumbleConnection.sendMessage "TextMessage",
      actor: mumbleConnection.sessionId
      channelId: [channel]
      message: "[I] #{from}: #{message}"


mumbleConnection = null

mumbleCredentials =
  key: fs.readFileSync MUMBLE_OPTS.key
  cert: fs.readFileSync MUMBLE_OPTS.cert

mumble.connect MUMBLE_OPTS.address, mumbleCredentials, (error, connection) ->
  console.log "[M] Connection established."
  console.log "[M] Authenticating..."

  connection.authenticate NICKNAME

  connection.on "initialized", ->
    console.log "[M] Connection initialized."
    mumbleConnection = connection

  # mirror text messages to IRC
  connection.on "textMessage", (data) ->
    user = connection.users[data.actor]
    console.log "[M] Message from #{user.name}: #{data.message}"
    console.log "[F] Forwarding to IRC..."
    ircClient.say IRC_OPTS.channel, "[M] #{user.name}: #{data.message}"
